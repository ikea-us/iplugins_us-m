## m.ikea.us - iplugins ##
**Remember:**  
Every push to master branch will do a sync to AWS S3 bucket. Too many syncs in short period will create a queue.  
*If you really, really like pushing*: Create another branch for you current project. And when all done -push to master branch.