$(document).ready(function () {
    if (document.URL.indexOf("https://m.ikea.com/us/en/catalog/products/") >= 0) {

        if (document.getElementsByClassName("ikea-breadcrumbs")[0].childNodes[1].innerHTML !== "Kitchen cabinets & a...");
        {
            if (document.getElementsByClassName("wrap-text ui-link-inherit")[0]) {
                {
                    (document.getElementsByClassName("wrap-text ui-link-inherit")[0]).innerHTML = "Assembly & documents";

                    var childNodeAssembly = document.getElementsByClassName("disclaimer-text ui-li-desc")[0]; 
                    var trDiv = document.createElement("div");
                    trDiv.innerHTML = '<div class="zipcodefinder">' +
                    '<div title="TaskRabbit is the convenient & fast way to get things done around the house.&#13Get help from carefully vetted and feedback rated Taskers." class="logoTask"> <img class="logoImgtaskRabbit" ' +
                    'src="https://ww8.ikea.com/ext/iplugins/en_US/development/plugins/taskrabbit/images/TaskRabbit_Logo_horizontal_lockup.svg"> </div> ' +
                    '<div class="textTask"> <div class="textInsideTask">Looking to book an assembly service?</div>' +
                    '<div class="textInsideTaskSmall">Find out if assembly service is available in your area</div> </div>' +
                    '<form><div class="inputTask"> <input placeholder="ZIP code" onkeypress="handle(event)" id="txtZip" name="zip" type="text" class="inputTaskZip"> ' +
                    '<input type="button" value="" class="buttonTask" onclick="IsValidZipCode(this.form.zip.value)">' +
                    '</form></div><div id="massageFromTask" class="massageFromTask"></div></div><hr>'; 
                    childNodeAssembly.parentNode.insertBefore(trDiv, childNodeAssembly);
                    console.log("task rabbit here");

                }
            }
        }
    }
}
)

function IsValidZipCode(zip) {
    var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
    if (isValid) {
        loadRequest();
    } else {
        document.getElementById("massageFromTask").innerHTML =  'Invalid ZipCode';
        // alert('Invalid ZipCode');
    }
}

function handle(e){
    if(e.keyCode === 13){
        e.preventDefault(); // Ensure it is only this code that rusn

        IsValidZipCode(document.getElementById("txtZip").value);
    }
}
